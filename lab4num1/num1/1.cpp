# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>

# define SIZE 256
 

void printWord(char *word)                      //  printWord 
{           
    int ch;
    while ((ch = *word++) != '\0')
        putchar(ch);
}
 

int getWords(char **mas, char *buf, int size)           // getWords
{
    int ch;
    int words_count = 0;
 
    while (*buf && words_count < size)
    {		
		if (*buf)
            mas[words_count++] = buf;
	}
	return words_count;
}




void pWords(char **mas, int words_count)
{
    int i = words_count - 1;
    for ( ; i > 0; --i)
    {
        int idx = rand() % (i+1);
        char *tmp = mas[i];
        mas[i] = mas[idx];
        mas[idx] = tmp;
    }
}
 

int main(void) // main
{
    char  buf[SIZE];
	char **mas;
	char MAX_WORDS;

    int words_count, i;
  
    printf("Enter a string: ");
    fgets(buf, SIZE, stdin);
    words_count = getWords(mas, buf, MAX_WORDS);
    pWords(mas, words_count);
    for (i = 0; i < words_count; ++i)
    {
        printWord(mas[i]);
        putchar('\n');
    }
    return 0;
}